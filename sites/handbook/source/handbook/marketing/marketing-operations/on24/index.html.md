---
layout: handbook-page-toc
title: "ON24"
description: "ON24 is a sales and marketing platform for digital engagement, delivering insights to drive ​revenue growth." 
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}


# About ON24

Page in progress - Recently purchased. Marketing Operations is in the process of integrating and implementing. Follow along in [epic](https://gitlab.com/groups/gitlab-com/-/epics/1800). 

## Provisioning

We have a limited number of seats. Before putting in an Access Request, please [open an issue](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/new?issuable_template=on24_access_request) for Marketing Operations to review your request.   Once your request is approved, then proceed to open an [Access Request]([create one](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request)

### User Roles
ON24 licenses are only required for people who are building webinar programs (presenters/speakers do not need a license). We have a limited number of licenses and adding more comes with a price.

**Platform Login: Administrative Access** (may also be referred to as a `license` or `seat`)
- The Platform Login has full access to Elite and all functionality, including the ability to create and edit webinars
- These logins are ONLY meant for those creating, setting up, and developing webinars / events
- The ability to edit / change logins is available

**Presenter Roles: URL/Link Access**
- Presenters may log in as Producer, Presenter, or a Q&A Moderator
- All three roles will access the event via a unique link for each event

The chart below outlines the user permissions for each role (Producer, Presenter, and Q&A moderator)

|   | Producer | Presenter | Q&A Moderator |
| - | -------- | --------- | -------------|
| Upload slide and videos | :white_check_mark: |  |  |
| Add poll questions | :white_check_mark: |  |  |
| Start/stop the webinar | :white_check_mark: |  |  |
| Arrange presentation materials | :white_check_mark: |  |  |
| Change webcam layouts | :white_check_mark: |  |  |
| Mute and disconnect presenters | :white_check_mark: |  |  |
| Advance slides | :white_check_mark: | :white_check_mark: |  |
| Push poll questions to the audience | :white_check_mark: | :white_check_mark: |  |
| Whiteboarding tools | :white_check_mark: | :white_check_mark: |  |
| Screenshare | :white_check_mark: | :white_check_mark: |  |
| Q&A | :white_check_mark: | :white_check_mark: | :white_check_mark: |
| Team chat | :white_check_mark: | :white_check_mark: | :white_check_mark: |

## Resources 

Please note, you will not be able to access these trainings until you have an ON24 login.  You can request access by opening a [MOps issue](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/new?issuable_template=on24_access_request).

- [Getting Started with Webcast Elite Certification](https://training.on24.com/standard-training)
- [Preparing Your Webinar Content](https://training.on24.com/preparing-your-webinar-content)
- [Presenter Certification & Training Program](https://training.on24.com/presenter-certification-new-improved)
- [ON24 Analytics 101](https://training.on24.com/analytics-101) and [Analytics 102](https://training.on24.com/analytics-102)
- [ON24 Knowledge Center](https://on24support.force.com/Support/s/knowledge)
- [ON24 Office Hours](https://training.on24.com/office-hours-webinars?next=%2Foffice-hours-webinars%2F869178) 

## Things to know

- Customize your webcast list view by using the 3 vertical lines as seen in the screenshot. Use this to see additional webcast information
-  Reminder emails, follow up emails and registration emails are all being handled by Marketo at this time. We may consider a different workflow as we become more accustomed to the platform

## Use of tags

We will add additional tags as we progress with the platform. Current tags in use:
  - `DO NOT USE`: self- explanatory hopefully 
  - `Template`: Use this webcast when cloning a new webinar event

## Customizing Your CTA

Each ON24 Console Templates will have a placeholder for a CTA. It is up to the DRI for the webcast to decide what the CTA is. In order to customize the CTA, following these steps:

1. [Log in to ON24](https://wcc.on24.com/webcast/login)
1. If you have not already, create a copy of the console template you wish to use by clicking the `create copy` icon on the right side of the page
1. Once you've created the copy, add the details of your webcast in the `Overview` section. Then select `Console Builder` from the left hand menu
1. Once in the console, click on the `CTA Placeholder` icon from the bottom menu. This icon looks like a pointer finger with signal bars above it. You will see `CTA Placeholder` when you hover over it. 
1. When the `CTA Placeholder` widget opens in the console, click on the gear in the right hand corner of the widget window. 
1. Click on `Attributes`. Update the `Name` field and select/deselect any of the options as needed. 
  1. Use one of this four options: `Talk to an Expert`, `Talk to Sales`, `Contact Us` or `Request a Demo`.
1. Click the gear again and select `Configuration`. Update the `Text`, `Button Text`, and `URL`. 

All changes are auto-saved. 

# Creating an ON24 Webcast

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://player.vimeo.com/video/348375866" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

Note: Live webinars, the start time, duration, and present type, cannot be changed 1-hour prior.  For simu-live and sim-2-live webinars, changes cannot be made 2 hours prior.  The drop-down menus will become inactive to ensure the webinar is properly set up and the technical infrastructure is prepared. For on-demand recordings, the start time represents when you want the presentation to be available for the audience to view. It can be changed as long as the scheduled start time has not already passed.

## Step-by-step guide
1. Log in to ON24 by visiting [https://wcc.on24.com/webcast/login](https://wcc.on24.com/webcast/login)
1. To create a brand new event, click on Create Webcast in the top corner of your library.

![Create a webcast screenshot](https://on24support.force.com/Support/servlet/rtaImage?eid=ka04U000000x4sp&feoid=00N4U000008YrFJ&refid=0EM4U000001eju0)

3. Enter a title for your webcast and choose your `Present Type`.

![Title and present type screenshot](https://on24support.force.com/Support/servlet/rtaImage?eid=ka04U000000x4sp&feoid=00N4U000008YrFJ&refid=0EM4U000001eju5)

4. Choose the date and time your event is to go live. When choosing your time zone, we recommend using one with a city or country listed.  The generic time zones do not update with time changes, such as Daylight Savings Time.
   - Note: If you change the time of a webinar, you will need to notify those already registered of the change.  Webcast Elite will not automatically notify them of the change.  You can access a list of registrants through the Reports URL.

![Time and date screenshot](https://on24support.force.com/Support/servlet/rtaImage?eid=ka04U000000x4sp&feoid=00N4U000008YrFJ&refid=0EM4U0000027uMX)   

5. Choose the duration of your event.  The duration can be set in 15-minute increments for any webinar under two hours.  Webinars longer than two hours may be set in 30-minute increments. The duration will display on the registration page and in the calendar reminders.

![Duration screenshot](https://on24support.force.com/Support/servlet/rtaImage?eid=ka04U000000x4sp&feoid=00N4U000008YrFJ&refid=0EM4U000001ek62) 

6. Choose the audience language for your event.  Choosing a language here will update the language for all of the system's static messages, buttons, and default email confirmations. You can switch languages as needed.  Changing the language here will not update any other webcast in your library, and will not automatically translate any custom text you previously added to the event.  

![Language screenshot](https://on24support.force.com/Support/servlet/rtaImage?eid=ka04U000000x4sp&feoid=00N4U000008YrFJ&refid=0EM4U000001ek66) 

7. Tag your event with up to 10 custom tags by clicking on the "+", to help you filter your webcast library.  These tags are for internal purposes and will not be shared with the public.  Previously used tags will appear in the drop-down. 

![Tagging screenshot](https://on24support.force.com/Support/servlet/rtaImage?eid=ka04U000000x4sp&feoid=00N4U000008YrFJ&refid=0EM4U000001ek6V) 

8. ON24 captures Benchmark data from all the webcasts run through Webcast Elite.  Help them organize that data by choosing a Category and Application, if you'd like.

![Benchmark data screenshot](https://on24support.force.com/Support/servlet/rtaImage?eid=ka04U000000x4sp&feoid=00N4U000008YrFJ&refid=0EM4U000001ek6f) 

9. Once you've filled in all the required fields, click Create.  The system will automatically generate an Event ID and Webcast URLs for your event. 

![Create screenshot](https://on24support.force.com/Support/servlet/rtaImage?eid=ka04U000000x4sp&feoid=00N4U000008YrFJ&refid=0EM4U000001ek6k) 

### Event IDs

Each webinar will be given a unique Event ID.  This number will be listed in your webcast library, as well as at the top of the screen throughout the platform.  Provide this number when reaching out to ON24 about the event. 

![Event ID screenshot1](https://on24support.force.com/Support/servlet/rtaImage?eid=ka04U000000x4sp&feoid=00N4U000008YrFJ&refid=0EM4U000001ek6p) 

![Event ID screenshot2](https://on24support.force.com/Support/servlet/rtaImage?eid=ka04U000000x4sp&feoid=00N4U000008YrFJ&refid=0EM4U000001ek6u) 

### Webcast URLs 

Click the globe icon in your webcast library or on the chain icon anywhere in an event to access the URLs for the webcast.

- Audience URL - The audience URL is used to promote your webinar and access the audience view of the webcast. This URL can be distributed for posting on various websites or for inclusion in email notifications.
- Reports URL - Use the Reports URL to access the online reports associated with the webcast.
- Preview URL - The Preview URL is used for previewing the audience console and test functionality before the webcast goes public.
- Present URL - Use the Present URL to access the presenter tool (either Presentation Manager XD (PMXD) or Elite Studio). One of these (depending on which you are using) will be the tool the presenting team will use to produce the webinar, manage the presentation elements (slides, polls, etc.), and interact with the attendees of the webcast.

### Console Builder

The Console Builder is at the heart of creating a custom webcast experience for your audience. The webcast console is what your audience will see when they attend your webcast. We currently have 2 console templates:
1. `Standard event template`, which includes a media player, slides, Q&A, resources, and speaker bio modules
1. `XL Media Player`, which is a media player with no slides. This template should be used if your presenter prefers to screenshare their slides, as opposed to uploading their deck into a separate module. This console also includes Q&A, resources, and speaker bio modules 

Follow these steps to apply a console template:
1. Click `Console Builder` from the left hand menu 
2. Open the Template Library, housed in the Select a Template drop-down
3. Double click on the name of the desired template

![Select a template screenshot](/handbook/marketing/marketing-operations/on24/template-screenshot.jpeg) 

4. When applying a console template, all setting and tools from the saved template will override the existing console. A confirmation dialogue will pop-up to prevent you from accidentally wiping out your current console.

![confirmation dialogue screenshot](/handbook/marketing/marketing-operations/on24/confirmation-screenshot.jpeg) 

### Connecting On24 Web Events to Marketo Programs

After completing the creation of an On24 web event, the next step is to connect the webinar to Marketo.
1. From the previous event set up, please be sure to have the `Event ID` and `Audience URL` handy.
1. Navigate to the Marketo template folder `Templates On24 - Webcast`. Located in this folder is the program template `YYYYMMDD_EventName_EventType_On24_template`. Make a copy of this program template in the appropriate folder.
1. Next step will be to connect the Marketo program to the On24 webcast. In the Smart Campaigns folder of the newly cloned program, add the On24 `Event ID` to the following smart campaigns on the `Added to ON24 Attendee` trigger filter:
    1. 04 On24 Processing - Attended
    1. 04 On24 Processing - Follow Up Requested
    1. 04 On24 Processing - No Show
    1. 04 On24 Processing - On Demand
1. Next activate the following smart trigger campaigns:
    1. 01 Registration Flow (choose single or multi)
    1. 00 Interesting Moments
    1. 01a Registration flow (single timeslot) or 01b Registration Flow (Multi-timeslot)
    1. 04 On24 Processing - Attended
    1. 04 On24 Processing - Follow Up Requested
    1. 04 On24 Processing - On Demand. 
        - Only activate this smart campaign if it is appropriate for the webinar, such as in the event the webinar will be left available for on-demand viewing. 
    1. 04 On24 Processing - No Show. 
        - No Show will not be activated as a trigger, but as a batch campaign scheduled to run 6 hours after the event.
1. Unlike other tools, the On24 room and Marketo program do not need to be connected via the `Event Partner` field on the Marketo program. All data transfer is done via the `Event ID` and smart campaigns.
1. Update the program tokens as needed within the program. Important tokens to review:
  - `my.webcastDate`, `my.webcastTitle` and `my.event location` are standard to update.
  - `my.on24URL`: This token needs to be updated as upon registration the registrant is sent an automatic email with the Audience URL attached to this token.
  - `my.On24password`: Update this with the webinar password. If no password was set up in the console, completely remove token from `registration confirmation` email as it is not necessary.
  - `my.bullet1` - `my.bullet4` may appear on the `registration confirmation` email so be sure to update either the tokens or the email template to accommodate 
  - Update others as needed
1. Please note the `Registration Flow` smart campaigns will send out the `Audience URL` for the event and have tokens arranged to share event passwords.


